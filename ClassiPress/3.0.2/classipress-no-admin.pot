# Translation of ClassiPress 3.0.2
# Copyright (C) 2010 ClassiPress
# This file can be distributed under the ClassiPress terms
#
#
msgid ""
msgstr ""
"Project-Id-Version: ClassiPress 3.0.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-04-23 20:15-0800\n"
"PO-Revision-Date: \n"
"Last-Translator: David Cowgill\n"
"Language-Team: ClassiPress\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: UNITED STATES\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:24
msgid "Whoops! Page Not Found."
msgstr ""

#: 404.php:26
msgid "The page or ad listing you are trying to reach no longer exists or has expired."
msgstr ""

#: 404.php:53
#: archive-default.php:111
#: search.php:101
msgid "Sponsored Links"
msgstr ""

#: archive-blog.php:29
msgid "Posted"
msgstr ""

#: archive-blog.php:29
msgid "by"
msgstr ""

#: archive-blog.php:29
msgid "in"
msgstr ""

#: archive-blog.php:29
msgid "No comments yet"
msgstr ""

#: archive-blog.php:29
msgid "1 comment"
msgstr ""

#: archive-blog.php:29
#, php-format
msgid "% comments"
msgstr ""

#: archive-blog.php:49
msgid "Sorry, no posts matched your criteria."
msgstr ""

#: archive-default.php:25
msgid "RSS Feed"
msgstr ""

#: archive-default.php:26
msgid "Listings for"
msgstr ""

#: archive-default.php:52
#: index-directory.php:127
#: index-directory.php:194
#: index-directory.php:255
#: index-standard.php:104
#: index-standard.php:171
#: index-standard.php:232
#: search.php:53
msgid "Category"
msgstr ""

#: archive-default.php:52
#: index-directory.php:127
#: index-directory.php:194
#: index-directory.php:255
#: index-standard.php:104
#: index-standard.php:171
#: index-standard.php:232
msgid "Listed"
msgstr ""

#: archive-default.php:75
msgid "There are not any listings in this category yet."
msgstr ""

#: archive-default.php:79
msgid "There are not any listings with this date."
msgstr ""

#: archive-default.php:84
#, php-format
msgid "There are not any listings by %s yet."
msgstr ""

#: archive-default.php:88
#: search.php:74
msgid "No listings found."
msgstr ""

#: author.php:36
msgid "About"
msgstr ""

#: author.php:58
#: sidebar-ad.php:100
msgid "Member Since:"
msgstr ""

#: author.php:60
msgid "Website:"
msgstr ""

#: author.php:62
msgid "Description:"
msgstr ""

#: author.php:72
msgid "Other items listed by"
msgstr ""

#: author.php:100
msgid "No ads by this poster yet."
msgstr ""

#: comments.php:11
msgid "Please do not load this page directly. Thanks!"
msgstr ""

#: comments.php:15
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#: comments.php:33
msgid "No Responses"
msgstr ""

#: comments.php:33
msgid "One Response"
msgstr ""

#: comments.php:33
msgid "% Responses"
msgstr ""

#: comments.php:33
msgid "to"
msgstr ""

#: comments.php:43
msgid "Older Comments"
msgstr ""

#: comments.php:45
msgid "Newer Comments"
msgstr ""

#: comments.php:55
msgid "Trackbacks/Pingbacks"
msgstr ""

#: comments.php:75
#: sidebar-blog.php:11
msgid "Comments"
msgstr ""

#: comments.php:76
msgid "Comments are closed."
msgstr ""

#: comments.php:89
#: comments.php:146
msgid "Leave a Reply"
msgstr ""

#: comments.php:89
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: comments.php:100
#, php-format
msgid "You must be <a href='%s'>logged in</a> to post a comment."
msgstr ""

#: comments.php:110
msgid "Logged in as"
msgstr ""

#: comments.php:110
msgid "Logout of this account"
msgstr ""

#: comments.php:110
msgid "Logout"
msgstr ""

#: comments.php:116
msgid "Name"
msgstr ""

#: comments.php:116
#: comments.php:123
msgid "(required)"
msgstr ""

#: comments.php:123
msgid "Email (will not be visible)"
msgstr ""

#: comments.php:130
msgid "Website"
msgstr ""

#: footer.php:10
#: header.php:103
msgid "Home"
msgstr ""

#: footer.php:35
msgid "All Rights Reserved."
msgstr ""

#: footer.php:42
msgid "Classified Ads Software"
msgstr ""

#: footer.php:42
msgid "Powered by"
msgstr ""

#: header.php:100
msgid "Post an Ad"
msgstr ""

#: header.php:104
#: sidebar-blog.php:82
msgid "Categories"
msgstr ""

#: index-directory.php:20
#: index-standard.php:20
msgid "Featured Listings"
msgstr ""

#: index-directory.php:68
msgid "Ad Categories"
msgstr ""

#: index-directory.php:87
#: index-directory.php:95
#: index-standard.php:65
#: index-standard.php:73
msgid "Just Listed"
msgstr ""

#: index-directory.php:88
#: index-directory.php:159
#: index-standard.php:66
#: index-standard.php:136
msgid "Most Popular"
msgstr ""

#: index-directory.php:89
#: index-directory.php:224
#: index-standard.php:67
#: index-standard.php:201
msgid "Random"
msgstr ""

#: index-directory.php:95
#: index-directory.php:159
#: index-directory.php:224
#: index-standard.php:73
#: index-standard.php:136
#: index-standard.php:201
msgid "Classified Ads"
msgstr ""

#: index-directory.php:144
#: index-directory.php:211
#: index-directory.php:272
#: index-standard.php:121
#: index-standard.php:188
#: index-standard.php:249
msgid "Sorry, no listings were found."
msgstr ""

#: page.php:34
msgid "Edit Page"
msgstr ""

#: page.php:51
msgid "Sorry, no pages matched your criteria."
msgstr ""

#: search.php:24
#, php-format
msgid "Search results for '%s'"
msgstr ""

#: search.php:53
msgid "Listed:"
msgstr ""

#: sidebar-ad.php:18
msgid "Map"
msgstr ""

#: sidebar-ad.php:20
msgid "Contact"
msgstr ""

#: sidebar-ad.php:21
msgid "Poster"
msgstr ""

#: sidebar-ad.php:59
msgid "You must be logged in to inquire about this ad."
msgstr ""

#: sidebar-ad.php:79
msgid "Information about the ad poster"
msgstr ""

#: sidebar-ad.php:82
msgid "Listed by:"
msgstr ""

#: sidebar-blog.php:10
msgid "Popular"
msgstr ""

#: sidebar-blog.php:12
msgid "Tags"
msgstr ""

#: sidebar-page.php:11
#: sidebar.php:54
msgid "BlogRoll"
msgstr ""

#: sidebar-page.php:25
#: sidebar.php:68
msgid "Meta"
msgstr ""

#: sidebar-user.php:10
msgid "User Options"
msgstr ""

#: sidebar-user.php:15
msgid "My Dashboard"
msgstr ""

#: sidebar-user.php:16
msgid "Edit Profile"
msgstr ""

#: sidebar-user.php:17
msgid "WordPress Admin"
msgstr ""

#: sidebar-user.php:18
msgid "Log Out"
msgstr ""

#: sidebar-user.php:33
msgid "Account Information"
msgstr ""

#: sidebar-user.php:41
msgid "Twitter"
msgstr ""

#: sidebar-user.php:42
msgid "Facebook"
msgstr ""

#: sidebar-user.php:59
msgid "Account Statistics"
msgstr ""

#: sidebar-user.php:70
msgid "Live Listings:"
msgstr ""

#: sidebar-user.php:71
msgid "Pending Listings:"
msgstr ""

#: sidebar-user.php:72
msgid "Offline Listings:"
msgstr ""

#: sidebar-user.php:73
msgid "Total Listings:"
msgstr ""

#: sidebar.php:13
msgid "Join Now!"
msgstr ""

#: sidebar.php:32
msgid "Sub Categories"
msgstr ""

#: single-blog.php:41
#: single-default.php:125
msgid "total views"
msgstr ""

#: single-blog.php:41
#: single-default.php:125
msgid "so far today"
msgstr ""

#: single-blog.php:45
msgid "Edit Ad"
msgstr ""

#: single-default.php:59
msgid "Location"
msgstr ""

#: single-default.php:60
msgid "Phone"
msgstr ""

#: single-default.php:63
msgid "URL"
msgstr ""

#: single-default.php:66
#: single-default.php:80
msgid "Expires"
msgstr ""

#: single-default.php:129
msgid "Edit Listing"
msgstr ""

#: single-default.php:143
msgid "Sorry, no listings matched your criteria."
msgstr ""

#: tpl-add-new-confirm.php:77
msgid "View your new ad"
msgstr ""

#: tpl-add-new-confirm.php:111
msgid "Thank You!"
msgstr ""

#: tpl-add-new-confirm.php:115
msgid "Your payment has been processed and your ad listing should now be live."
msgstr ""

#: tpl-add-new-confirm.php:117
msgid "Visit your dashboard to make any changes to your ad listing or profile."
msgstr ""

#: tpl-add-new-confirm.php:126
msgid "An Error Has Occurred"
msgstr ""

#: tpl-add-new-confirm.php:130
msgid "This ad has already been published or you do not have permission to activate this ad. Please contact the site admin if you are experiencing any issues."
msgstr ""

#: tpl-dashboard.php:99
#, php-format
msgid "%s's Dashboard"
msgstr ""

#: tpl-dashboard.php:101
msgid "Below you will find a listing of all your classified ads. Click on one of the options to perform a specific task. If you have any questions, please contact the site administrator."
msgstr ""

#: tpl-dashboard.php:107
msgid "Title"
msgstr ""

#: tpl-dashboard.php:108
msgid "Status"
msgstr ""

#: tpl-dashboard.php:109
msgid "Options"
msgstr ""

#: tpl-dashboard.php:145
msgid "Live"
msgstr ""

#: tpl-dashboard.php:148
msgid "pause ad"
msgstr ""

#: tpl-dashboard.php:157
msgid "awaiting approval"
msgstr ""

#: tpl-dashboard.php:165
msgid "awaiting payment"
msgstr ""

#: tpl-dashboard.php:178
msgid "ended"
msgstr ""

#: tpl-dashboard.php:186
msgid "offline"
msgstr ""

#: tpl-dashboard.php:189
msgid "restart ad"
msgstr ""

#: tpl-dashboard.php:238
msgid "Relist Ad"
msgstr ""

#: tpl-dashboard.php:271
msgid "You currently have no classified ads."
msgstr ""

#: tpl-edit-item.php:126
msgid "Edit Your Ad"
msgstr ""

#: tpl-edit-item.php:131
msgid "Edit the fields below and click save to update your ad. Your changes will be updated instantly on the site."
msgstr ""

#: tpl-edit-item.php:207
msgid "Images"
msgstr ""

#: tpl-edit-item.php:207
msgid "Sorry, image editing is not supported for this ad."
msgstr ""

#: tpl-edit-item.php:216
msgid "Cancel"
msgstr ""

#: tpl-edit-item.php:217
msgid "Update Ad &raquo;"
msgstr ""

#: tpl-edit-item.php:230
msgid "You have entered an invalid ad id or do not have permission to edit that ad."
msgstr ""

#: tpl-full-width.php:41
msgid "No content found."
msgstr ""

#: tpl-profile.php:106
#: tpl-profile.php:236
msgid "Strength indicator"
msgstr ""

#: tpl-profile.php:107
msgid "Very weak"
msgstr ""

#: tpl-profile.php:108
msgid "Weak"
msgstr ""

#: tpl-profile.php:109
msgid "Medium"
msgstr ""

#: tpl-profile.php:110
msgid "Strong"
msgstr ""

#: tpl-profile.php:131
#, php-format
msgid "%s's Profile"
msgstr ""

#: tpl-profile.php:141
msgid "Your profile has been updated."
msgstr ""

#: tpl-profile.php:160
msgid "Username"
msgstr ""

#: tpl-profile.php:164
msgid "First Name"
msgstr ""

#: tpl-profile.php:168
msgid "Last Name"
msgstr ""

#: tpl-profile.php:172
msgid "Nickname"
msgstr ""

#: tpl-profile.php:176
msgid "Display Name"
msgstr ""

#: tpl-profile.php:199
msgid "Email"
msgstr ""

#: tpl-profile.php:211
msgid "About Me"
msgstr ""

#: tpl-profile.php:220
msgid "New Password"
msgstr ""

#: tpl-profile.php:223
msgid "Leave this field blank unless you would like to change your password."
msgstr ""

#: tpl-profile.php:227
msgid "Password Again"
msgstr ""

#: tpl-profile.php:230
msgid "Type your new password again."
msgstr ""

#: tpl-profile.php:237
msgid "Your password should be at least seven characters long."
msgstr ""

#: tpl-profile.php:252
msgid "Thumbnail"
msgstr ""

#: tpl-profile.php:264
msgid "Delete existing photo?"
msgstr ""

#: tpl-profile.php:274
msgid "Update Profile &raquo;"
msgstr ""

#: includes/sidebar-blog-posts.php:32
msgid "From the Blog"
msgstr ""

#: includes/sidebar-blog-posts.php:50
msgid "on"
msgstr ""

#: includes/sidebar-blog-posts.php:58
msgid "There are no blog articles yet."
msgstr ""

#: includes/sidebar-comments.php:33
msgid "Comment on article "
msgstr ""

#: includes/sidebar-contact.php:22
msgid "Your message has been sent!"
msgstr ""

#: includes/sidebar-contact.php:24
msgid "ERROR: Incorrect captcha answer"
msgstr ""

#: includes/sidebar-contact.php:36
msgid "To inquire about this ad listing, complete the form below to send a message to the ad poster."
msgstr ""

#: includes/sidebar-contact.php:40
msgid "Name:"
msgstr ""

#: includes/sidebar-contact.php:46
msgid "Email:"
msgstr ""

#: includes/sidebar-contact.php:52
msgid "Subject:"
msgstr ""

#: includes/sidebar-contact.php:53
msgid "Re:"
msgstr ""

#: includes/sidebar-contact.php:58
msgid "Message:"
msgstr ""

#: includes/sidebar-contact.php:74
msgid "Sum of"
msgstr ""

#: includes/sidebar-contact.php:80
msgid "Send Inquiry"
msgstr ""

#: includes/sidebar-gmap.php:3
msgid "Item Location"
msgstr ""

#: includes/sidebar-gmap.php:62
#: includes/sidebar-gmap.php:69
#: includes/sidebar-gmap.php:76
msgid "Directions:"
msgstr ""

#: includes/sidebar-gmap.php:62
#: includes/sidebar-gmap.php:69
#: includes/sidebar-gmap.php:76
msgid "To here"
msgstr ""

#: includes/sidebar-gmap.php:62
#: includes/sidebar-gmap.php:69
#: includes/sidebar-gmap.php:76
msgid "From here"
msgstr ""

#: includes/sidebar-gmap.php:63
msgid "Start address:"
msgstr ""

#: includes/sidebar-gmap.php:65
#: includes/sidebar-gmap.php:72
msgid "Get Directions"
msgstr ""

#: includes/sidebar-gmap.php:70
msgid "End address:"
msgstr ""

#: includes/sidebar-gmap.php:93
msgid "Address was not found"
msgstr ""

#: includes/sidebar-gmap.php:108
msgid "Sorry, the Google Maps API is not compatible with this browser"
msgstr ""

#: includes/theme-comments.php:28
msgid "Edit"
msgstr ""

#: includes/theme-comments.php:43
msgid "Your comment is awaiting moderation."
msgstr ""

#: includes/theme-comments.php:51
msgid "Reply"
msgstr ""

#: includes/theme-comments.php:52
msgid "Log in to reply."
msgstr ""

#: includes/theme-emails.php:31
msgid "New Ad Submission"
msgstr ""

#: includes/theme-emails.php:32
msgid "ClassiPress Admin"
msgstr ""

#: includes/theme-emails.php:38
msgid "Dear Admin,"
msgstr ""

#: includes/theme-emails.php:39
#, php-format
msgid "The following ad listing has just been submitted on your %s website."
msgstr ""

#: includes/theme-emails.php:40
#: includes/theme-emails.php:87
msgid "Ad Details"
msgstr ""

#: includes/theme-emails.php:41
#: includes/theme-emails.php:46
#: includes/theme-emails.php:88
#: includes/theme-emails.php:93
msgid "-----------------"
msgstr ""

#: includes/theme-emails.php:42
#: includes/theme-emails.php:89
msgid "Title: "
msgstr ""

#: includes/theme-emails.php:43
#: includes/theme-emails.php:90
msgid "Category: "
msgstr ""

#: includes/theme-emails.php:44
msgid "Author: "
msgstr ""

#: includes/theme-emails.php:47
msgid "Preview Ad: "
msgstr ""

#: includes/theme-emails.php:48
#, php-format
msgid "Edit Ad: %s"
msgstr ""

#: includes/theme-emails.php:49
#: includes/theme-emails.php:96
#: includes/theme-emails.php:154
#: includes/theme-emails.php:176
msgid "Regards,"
msgstr ""

#: includes/theme-emails.php:50
msgid "ClassiPress"
msgstr ""

#: includes/theme-emails.php:82
#, php-format
msgid "Your Ad Submission on %s"
msgstr ""

#: includes/theme-emails.php:83
#: includes/theme-emails.php:147
#: includes/theme-emails.php:166
#, php-format
msgid "%s Admin"
msgstr ""

#: includes/theme-emails.php:85
#: includes/theme-emails.php:149
#: includes/theme-emails.php:168
#, php-format
msgid "Hi %s,"
msgstr ""

#: includes/theme-emails.php:86
#, php-format
msgid "Thank you for your recent submission! Your ad listing has been submitted for review and will not appear live on our site until it has been approved. Below you will find a summary of your ad listing on the %s website."
msgstr ""

#: includes/theme-emails.php:91
msgid "Status: "
msgstr ""

#: includes/theme-emails.php:94
msgid "You may check the status of your ad(s) at anytime by logging into your dashboard."
msgstr ""

#: includes/theme-emails.php:97
#: includes/theme-emails.php:155
#: includes/theme-emails.php:177
#, php-format
msgid "Your %s Team"
msgstr ""

#: includes/theme-emails.php:146
msgid "Your Ad Has Been Approved"
msgstr ""

#: includes/theme-emails.php:150
#, php-format
msgid "Your ad listing, \"%s\" has been approved and is now live on our site."
msgstr ""

#: includes/theme-emails.php:152
msgid "You can view your ad by clicking on the following link:"
msgstr ""

#: includes/theme-emails.php:165
msgid "Your Ad Has Expired"
msgstr ""

#: includes/theme-emails.php:169
#, php-format
msgid "Your ad listing, \"%s\" has expired."
msgstr ""

#: includes/theme-emails.php:172
msgid "If you would like to relist your ad, please visit your dashboard and click the \"relist\" link."
msgstr ""

#: includes/theme-emails.php:218
#, php-format
msgid "Someone is interested in your ad listing: %s"
msgstr ""

#: includes/theme-emails.php:222
#, php-format
msgid "This message was sent from %s"
msgstr ""

#: includes/theme-functions.php:63
#: includes/theme-functions.php:65
msgid "Welcome,"
msgstr ""

#: includes/theme-functions.php:63
msgid "Log out"
msgstr ""

#: includes/theme-functions.php:65
msgid "visitor!"
msgstr ""

#: includes/theme-functions.php:65
#: includes/theme-login.php:326
#: includes/theme-login.php:355
#: includes/theme-login.php:396
msgid "Register"
msgstr ""

#: includes/theme-functions.php:65
#: includes/theme-login.php:157
#: includes/theme-login.php:203
msgid "Login"
msgstr ""

#: includes/theme-functions.php:175
#, php-format
msgid "%s ago"
msgstr ""

#: includes/theme-functions.php:224
msgid "No ad details found."
msgstr ""

#: includes/theme-functions.php:321
#: includes/theme-functions.php:425
msgid "Image "
msgstr ""

#: includes/theme-functions.php:381
msgid "image"
msgstr ""

#: includes/theme-functions.php:553
#, php-format
msgid "exceeds the %s KB limit by %s KB. Please go back and upload a smaller image."
msgstr ""

#: includes/theme-functions.php:556
msgid "is not a valid image type (.gif, .jpg, .png). Please go back and upload a different image."
msgstr ""

#: includes/theme-functions.php:632
msgid "Image"
msgstr ""

#: includes/theme-functions.php:687
msgid "Add Image"
msgstr ""

#: includes/theme-functions.php:695
#, php-format
msgid "You are allowed %s image(s) per ad."
msgstr ""

#: includes/theme-functions.php:695
msgid "KB max file size per image."
msgstr ""

#: includes/theme-functions.php:695
msgid "Check the box next to each image you wish to delete."
msgstr ""

#: includes/theme-functions.php:720
msgid "Error in deleting the image."
msgstr ""

#: includes/theme-functions.php:990
msgid "Your ad has been successfully updated."
msgstr ""

#: includes/theme-functions.php:990
msgid "Return to my dashboard"
msgstr ""

#: includes/theme-functions.php:994
msgid "There was an error trying to update your ad."
msgstr ""

#: includes/theme-functions.php:1085
msgid "days"
msgstr ""

#: includes/theme-functions.php:1086
msgid "day"
msgstr ""

#: includes/theme-functions.php:1087
msgid "hours"
msgstr ""

#: includes/theme-functions.php:1088
msgid "hour"
msgstr ""

#: includes/theme-functions.php:1089
msgid "mins"
msgstr ""

#: includes/theme-functions.php:1090
msgid "min"
msgstr ""

#: includes/theme-functions.php:1091
msgid "secs"
msgstr ""

#: includes/theme-functions.php:1092
msgid "remaining"
msgstr ""

#: includes/theme-functions.php:1093
msgid "This ad has expired"
msgstr ""

#: includes/theme-functions.php:1183
msgid "Search results for"
msgstr ""

#: includes/theme-functions.php:1193
msgid "Articles posted by"
msgstr ""

#: includes/theme-functions.php:1196
msgid "Error 404"
msgstr ""

#: includes/theme-functions.php:1201
msgid "Page"
msgstr ""

#: includes/theme-functions.php:1255
msgid "No matches found"
msgstr ""

#: includes/theme-functions.php:1364
msgid "Page %CURRENT_PAGE% of %TOTAL_PAGES%"
msgstr ""

#: includes/theme-functions.php:1367
msgid "&lsaquo;&lsaquo; First"
msgstr ""

#: includes/theme-functions.php:1368
msgid "Last &rsaquo;&rsaquo;"
msgstr ""

#: includes/theme-functions.php:1369
msgid "&rsaquo;&rsaquo;"
msgstr ""

#: includes/theme-functions.php:1370
msgid "&lsaquo;&lsaquo;"
msgstr ""

#: includes/theme-login.php:101
msgid "Register at "
msgstr ""

#: includes/theme-login.php:105
msgid "Retrieve your lost password for "
msgstr ""

#: includes/theme-login.php:110
msgid "Login at "
msgstr ""

#: includes/theme-login.php:117
msgid "Your Profile at "
msgstr ""

#: includes/theme-login.php:161
msgid "Cookies are blocked or not supported by your browser. You must enable cookies to continue."
msgstr ""

#: includes/theme-login.php:164
msgid "You are now logged out."
msgstr ""

#: includes/theme-login.php:167
msgid "User registration is currently not allowed."
msgstr ""

#: includes/theme-login.php:170
msgid "Check your email for the confirmation link."
msgstr ""

#: includes/theme-login.php:173
msgid "Check your email for your new password."
msgstr ""

#: includes/theme-login.php:176
msgid "Registration complete. Please check your e-mail."
msgstr ""

#: includes/theme-login.php:184
msgid "Please complete the fields below to login."
msgstr ""

#: includes/theme-login.php:188
msgid "Username:"
msgstr ""

#: includes/theme-login.php:191
msgid "Password:"
msgstr ""

#: includes/theme-login.php:200
msgid "Remember me"
msgstr ""

#: includes/theme-login.php:243
msgid "Sorry, that key does not appear to be valid."
msgstr ""

#: includes/theme-login.php:257
msgid "Please enter your username or email address. A new password will be emailed to you."
msgstr ""

#: includes/theme-login.php:261
msgid "Username or Email:"
msgstr ""

#: includes/theme-login.php:267
msgid "Get New Password"
msgstr ""

#: includes/theme-login.php:321
msgid "<strong>ERROR</strong>: You didn't correctly enter the captcha, please try again."
msgstr ""

#: includes/theme-login.php:333
msgid "Complete the fields below to become a member. Your password will be emailed to you so make sure to use a valid email address. Once registration is complete, you will be able to submit your ads."
msgstr ""

#: includes/theme-login.php:346
msgid "Type the code above:"
msgstr ""

#: includes/theme-login.php:393
msgid "Log in"
msgstr ""

#: includes/theme-login.php:399
msgid "Password Lost and Found"
msgstr ""

#: includes/theme-login.php:399
msgid "Lost your password?"
msgstr ""

#: includes/theme-profile.php:28
msgid "Please enter your Twitter username."
msgstr ""

#: includes/theme-profile.php:37
msgid "Please enter your Facebook username."
msgstr ""

#: includes/theme-profile.php:47
msgid "PayPal Email"
msgstr ""

#: includes/theme-profile.php:51
msgid "Please enter your PayPal email address."
msgstr ""

#: includes/theme-security.php:32
msgid "Access Denied."
msgstr ""

#: includes/theme-security.php:38
msgid "Access Denied. Your site administrator has blocked your access to the WordPress back-office."
msgstr ""

#: includes/theme-sidebars.php:9
msgid "Main Sidebar"
msgstr ""

#: includes/theme-sidebars.php:17
msgid "Ad Sidebar"
msgstr ""

#: includes/theme-sidebars.php:25
msgid "Page Sidebar"
msgstr ""

#: includes/theme-sidebars.php:33
msgid "Blog Sidebar"
msgstr ""

#: includes/theme-sidebars.php:41
msgid "User Sidebar"
msgstr ""

#: includes/theme-sidebars.php:49
msgid "Footer"
msgstr ""

#: includes/theme-stats.php:151
msgid "Visited 1 time"
msgstr ""

#: includes/theme-stats.php:274
msgid "No ads viewed yet."
msgstr ""

#: includes/theme-widgets.php:47
msgid "What are you looking for?"
msgstr ""

#: includes/theme-widgets.php:49
msgid "All Categories"
msgstr ""

#: includes/theme-widgets.php:51
msgid "Search"
msgstr ""

#: includes/theme-widgets.php:72
msgid "Filter by City"
msgstr ""

#: includes/theme-widgets.php:92
msgid "All"
msgstr ""

#: includes/theme-widgets.php:107
msgid "No cities found."
msgstr ""

#: includes/theme-widgets.php:137
msgid "Site Sponsors"
msgstr ""

#: includes/theme-widgets.php:171
msgid "Your sidebar ad search box"
msgstr ""

#: includes/theme-widgets.php:172
msgid "Ad Search Box"
msgstr ""

#: includes/theme-widgets.php:178
msgid "Search Classified Ads"
msgstr ""

#: includes/theme-widgets.php:196
#: includes/theme-widgets.php:234
#: includes/theme-widgets.php:271
msgid "Title:"
msgstr ""

#: includes/theme-widgets.php:209
msgid "Your sidebar top ads today"
msgstr ""

#: includes/theme-widgets.php:210
msgid "Top Ads Today"
msgstr ""

#: includes/theme-widgets.php:216
msgid "Popular Ads Today"
msgstr ""

#: includes/theme-widgets.php:246
msgid "Your sidebar top ads overall"
msgstr ""

#: includes/theme-widgets.php:247
msgid "Top Ads Overall"
msgstr ""

#: includes/theme-widgets.php:253
msgid "Popular Ads Overall"
msgstr ""

#: includes/form/step-functions.php:45
msgid "Select"
msgstr ""

#: includes/form/step-functions.php:125
msgid "KB max file size per image"
msgstr ""

#: includes/form/step-functions.php:146
msgid "Featured Listing"
msgstr ""

#: includes/form/step-functions.php:149
msgid "Your listing will appear in the featured slider section at the top of the front page."
msgstr ""

#: includes/form/step-functions.php:163
msgid "Ad Package"
msgstr ""

#: includes/form/step-functions.php:189
msgid "Error: no ad pack has been defined."
msgstr ""

#: includes/form/step-functions.php:199
#: includes/form/step-functions.php:374
msgid "Payment Method"
msgstr ""

#: includes/form/step-functions.php:203
msgid "PayPal"
msgstr ""

#: includes/form/step-functions.php:204
msgid "Google Checkout"
msgstr ""

#: includes/form/step-functions.php:205
msgid "2Checkout"
msgstr ""

#: includes/form/step-functions.php:206
msgid "Authorize.net"
msgstr ""

#: includes/form/step-functions.php:207
msgid "Chronopay"
msgstr ""

#: includes/form/step-functions.php:208
msgid "MoneyBookers"
msgstr ""

#: includes/form/step-functions.php:320
msgid "ERROR: no results found for the default ad form."
msgstr ""

#: includes/form/step-functions.php:363
#, php-format
msgid "ERROR: The form template for form ID %s does not exist or the session variable is empty."
msgstr ""

#: includes/form/step-functions.php:381
msgid "Ad Listing Fee"
msgstr ""

#: includes/form/step-functions.php:382
msgid "FREE"
msgstr ""

#: includes/form/step-functions.php:388
msgid "Featured Listing Fee"
msgstr ""

#: includes/form/step-functions.php:398
msgid "Total Amount Due"
msgstr ""

#: includes/form/step-functions.php:402
msgid "--"
msgstr ""

#: includes/form/step-functions.php:421
msgid "Price depends on category"
msgstr ""

#: includes/form/step-functions.php:425
msgid "Price depends on ad package selected"
msgstr ""

#: includes/form/step-functions.php:429
#, php-format
msgid "% of your ad listing price"
msgstr ""

#: includes/form/step-functions.php:434
#: includes/form/step-functions.php:439
msgid "Free"
msgstr ""

#: includes/form/step-functions.php:500
#, php-format
msgid "ERROR: no ad packs found for ID %s."
msgstr ""

#: includes/form/step1.php:21
msgid "Submit Your Listing"
msgstr ""

#: includes/form/step1.php:46
msgid "Cost Per Listing"
msgstr ""

#: includes/form/step1.php:52
msgid "Select a Category:"
msgstr ""

#: includes/form/step1.php:58
#: includes/form/step1.php:62
msgid "Select one"
msgstr ""

#: includes/form/step1.php:68
msgid "Go"
msgstr ""

#: includes/form/step1.php:91
msgid "(change)"
msgstr ""

#: includes/form/step1.php:97
msgid "Continue &rsaquo;&rsaquo;"
msgstr ""

#: includes/form/step2.php:94
msgid "Review Your Listing"
msgstr ""

#: includes/form/step2.php:130
msgid "By clicking the proceed button below, you agree to our terms and conditions."
msgstr ""

#: includes/form/step2.php:132
msgid "Your IP address has been logged for security purposes:"
msgstr ""

#: includes/form/step2.php:136
msgid "Go back"
msgstr ""

#: includes/form/step2.php:137
msgid "Proceed "
msgstr ""

#: includes/form/step2.php:157
msgid "Go Back"
msgstr ""

#: includes/form/step3.php:31
msgid "Final Step"
msgstr ""

#: includes/form/step3.php:31
msgid "Ad Listing Received"
msgstr ""

#: includes/form/step3.php:61
msgid "Thank you! Your ad listing has been submitted for review."
msgstr ""

#: includes/form/step3.php:62
msgid "You can check the status by viewing your dashboard."
msgstr ""

#: includes/form/step3.php:66
msgid "Thank you! Your ad listing has been submitted and is now live."
msgstr ""

#: includes/form/step3.php:68
msgid "View your new ad listing."
msgstr ""

#: includes/form/step3.php:100
msgid "Your session has expired or you are trying to submit a duplicate ad. Please start over."
msgstr ""

#: includes/gateways/gateway.php:11
msgid "Please wait while we redirect you to our payment page."
msgstr ""

#: includes/gateways/gateway.php:15
msgid "(Click the button below if you are not automatically redirected within 5 seconds.)"
msgstr ""

#: includes/gateways/gateway.php:57
msgid "Error: No payment gateway can be found or your session has timed out."
msgstr ""

#: includes/gateways/paypal/paypal.php:28
#, php-format
msgid "Classified ad listing on %s for %s days"
msgstr ""

#: includes/gateways/paypal/paypal.php:41
msgid "Click here to publish your ad on"
msgstr ""

#: includes/gateways/paypal/paypal.php:51
msgid "Continue"
msgstr ""

